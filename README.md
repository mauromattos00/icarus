# ATech Icarus

## Resumo

Icarus é o crawler que realiza a coleta e match de produtos de várias marcas
de acordo com um EAN fornecido através do Google Shopping.

**O Google Shopping disponibiliza a pesquisa de produtos atráves de seu EAN**, 
então para cada EAN da planilha fornecida, é recuperado o produto que melhor corresponse ao EAN daquela pesquisa.
Na página do produto são coletadas todas suas informações relevantes e a lista de sellers
que vendem o produto, o que configura o match.


## Instalação

### Dependências

- Pymongo
- Scrapy

Instale as três dependências para que a aplicação funcione corretamente.
```
pip install pymongo
pip install scrapy

```

# -*- coding: utf-8 -*-

# Define here the models for your scraped items

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst


class ProductItem(scrapy.Item):
    spider = scrapy.Field(
        output_processor=TakeFirst()
    )
    not_found = scrapy.Field(
        output_processor=TakeFirst()
    )
    batch_id = scrapy.Field(
        input_processor=MapCompose(),
        output_processor=TakeFirst()
    )
    created_at = scrapy.Field(
        output_processor=TakeFirst()
    )
    updated_at = scrapy.Field(
        output_processor=TakeFirst()
    )
    ean = scrapy.Field(
        input_processor=MapCompose(),
        output_processor=TakeFirst()
    )
    name = scrapy.Field(
        input_processor=MapCompose(),
        output_processor=TakeFirst()
    )
    description = scrapy.Field(
        input_processor=MapCompose(),
        output_processor=TakeFirst()
    )
    sellers = scrapy.Field(
        input_processor=MapCompose()
    )


class CSVItem(scrapy.Item):
    ean = scrapy.Field(
        output_processor=TakeFirst()
    )
    spider = scrapy.Field(
        output_processor=TakeFirst()
    )
    created_at = scrapy.Field(
        output_processor=TakeFirst()
    )


class GFKItem(scrapy.Item):
    query = scrapy.Field()
    ean = scrapy.Field()
    flags = scrapy.Field()
    date = scrapy.Field()
    sellers = scrapy.Field()

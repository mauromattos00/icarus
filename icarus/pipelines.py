# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from datetime import datetime
from scrapy.conf import settings


class IcarusPipeline(object):
    def process_item(self, item, spider):
        return item


class DocumentDBPipeline(object):

    def __init__(self):
        client = pymongo.MongoClient(
            "atech-cluster.cluster-ctzxecurmuib.us-east-1.docdb.amazonaws.com:27017",
            ssl=True,
            ssl_ca_certs='rds-combined-ca-bundle.pem',
            username='atechadmin',
            password='atechb2w'
        )
        self.db = client[settings['DB_NAME']]
        self.products = self.db['products']

    def process_item(self, item, spider):

        if item[u'spider'] == 'icarusnotfound':
            self.products.update_one(
                {'ean': item[u'ean']},
                {'$set': {'not_found': True}}
            )

        elif item[u'spider'] == 'icarus':
            self.products.update_one(
                {'ean': item[u'ean']},
                {'$set': {
                    'sellers': item['sellers'],
                    'name': item[u'name'],
                    'description': item[u'description'],
                    'updated_at': datetime.now()
                }}
            )

        elif item[u'spider'] == 'fromcsv':
            self.products.insert_one({
                'ean': item[u'ean'],
                'created_at': str(datetime.now())
            })

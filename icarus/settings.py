# -*- coding: utf-8 -*-

BOT_NAME = 'Icarus'

SPIDER_MODULES = ['icarus.spiders']
NEWSPIDER_MODULE = 'icarus.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT_LIST = 'C:/Users/mauro.mattos/Documents/repositorios/icarus/useragents.txt'
DUPEFILTER_CLASS = 'scrapy.dupefilters.BaseDupeFilter'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 1

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 2
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 1
CONCURRENT_REQUESTS_PER_IP = 1

# Disable cookies (enabled by default)
# COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
    'icarus.middlewares.IcarusSpiderMiddleware': 543,
}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'icarus.middlewares.RandomUserAgentMiddleware': 400,
    'icarus.middlewares.RandomProxyMiddleware': 450,
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 540,
    'icarus.middlewares.IcarusDownloaderMiddleware': 543,
    # 'scrapy_proxies.RandomProxy': 550,
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Proxy list containing entries like
# http://host1:port
# http://username:password@host2:port
# http://host3:port
# ...
# PROXY_LIST = '/home/mauro.mattos/Git/icarus/proxylist.txt'

# Proxy mode
# 0 = Every requests have different proxy
# 1 = Take only one proxy from the list and assign it to every requests
# 2 = Put a custom proxy to use in the settings
# PROXY_MODE = 0

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'icarus.pipelines.DocumentDBPipeline': 300,
}

# Crawl of Ajax "crawlable pages"
AJAXCRAWL_ENABLED = True

# RETRIES
RETRY_ENABLED = True

# Redirect links
REDIRECT_ENABLED = True

# MongoDB Settings
DB_NAME = "icarus"
MONGODB_PORT = 27017
MONGODB_DB = "icarus"
MONGODB_COLLECTION = "products"


AWS_REGION = 'us-east-1'

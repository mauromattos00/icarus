# -*- coding: utf-8 -*-
from __future__ import absolute_import
import scrapy
import boto3
import pymongo
from scrapy.conf import settings
try:
    from urllib.parse import urlparse, parse_qs
except ImportError:
    from urlparse import urlparse, parse_qs


class NormalizedSpider(scrapy.Spider):

    name = 'normalized'

    custom_settings = {
        'CONCURRENT_REQUESTS': 16,
        'DOWNLOAD_DELAY': 1,
        'CONCURRENT_REQUESTS_PER_IP': 16,
        'DOWNLOAD_TIMEOUT': 5
    }

    allowed_domains = ['google.com']
    start_urls = ['http://google.com/']

    # Recupera produtos que contém urls do Google em algum de seus sellers
    client = pymongo.MongoClient(
        "atech-cluster.cluster-ctzxecurmuib.us-east-1.docdb.amazonaws.com:27017",
        ssl=True,
        ssl_ca_certs='rds-combined-ca-bundle.pem',
        username='atechadmin',
        password='atechb2w'
    )
    db = client[settings['DB_NAME']]
    collection = db['products']
    response = collection.find({
        'sellers': {'$exists': True},
        '$or': [
            {'sellers.url': {'$regex': ".*www.google.com.*"}},
            {'sellers.url': {'$regex': ".*www.googleadservices.com.*"}}
        ]
    })

    def start_requests(self):
        for product in self.response:
            for seller in product['sellers']:
                # Verifica se a URL do seller é uma URL Google
                if 'google.com' in seller['url']:
                    product['spider'] = 'normalized'
                    yield scrapy.Request(
                        url=seller['url'],
                        callback=self.parse_seller,
                        errback=self.parse_seller_err,
                        meta={'product': product, 'seller': seller}
                    )
                else:
                    pass

    def parse_seller(self, response):
        seller = response.meta['seller']
        product = response.meta['product']
        urls_redirect = response.request.meta.get('redirect_urls')

        if urls_redirect:
            for url_redirect in urls_redirect:
                parsed = urlparse(url_redirect)

                if 'ds_dest_url=http' in url_redirect:
                    last_param = parse_qs(parsed.query)['ds_dest_url'][0]
                elif 'redirect_url=http' in url_redirect:
                    last_param = parse_qs(parsed.query)['redirect_url'][0]
                elif 'https://tracker2' in url_redirect:
                    last_param = response.url
                elif 'adurl=http' in url_redirect:
                    last_param = parse_qs(parsed.query)['adurl'][0]
                elif 'url=http' in url_redirect:
                    last_param = parse_qs(parsed.query)['url'][0]
                else:
                    last_param = response.url
        else:
            last_param = response.url

        try:
            url_normalized = last_param.replace('%3F', '?').replace('%3D', '=').replace('%26', '&')
        except:
            url_normalized = last_param

        seller['url'] = url_normalized

        try:
            url_shorted = url_normalized[:url_normalized.index('?')]
        except:
            url_shorted = url_normalized

        collection = self.db['items_to_queue']
        collection.insert_one({
            'ean': product['ean'],
            'revisit': 'd-1',
            'url': url_shorted
        })
        yield product

    def parse_seller_err(self, response):

        seller = response.request.meta['seller']
        product = response.request.meta['product']

        seller['url'] = response.request.url

        yield product

# -*- coding: utf-8 -*-
from __future__ import absolute_import
from icarus.items import CSVItem
from scrapy.loader import ItemLoader
from scrapy.spiders import CSVFeedSpider


class FromcsvSpider(CSVFeedSpider):

    name = 'fromcsv'
    start_urls = [
        'https://raw.githubusercontent.com/brunocalixto/gshop/master/1p_full_1.csv'
    ]
    delimiter = ';'
    quotechar = '"'

    headers = ['EAN']

    def parse_row(self, response, row):

        csv_item = ItemLoader(item=CSVItem(), response=response)
        csv_item.add_value('ean', row['EAN'])
        csv_item.add_value('spider', 'fromcsv')
        yield csv_item.load_item()

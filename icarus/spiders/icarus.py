# -*- coding: utf-8 -*-
from __future__ import absolute_import
import pymongo
import scrapy
from scrapy.conf import settings
from scrapy.loader import ItemLoader
from icarus.items import ProductItem
try:
    from urllib.parse import urlparse, parse_qs
except ImportError:
    from urlparse import urlparse, parse_qs


class IcarusSpider(scrapy.Spider):
    # Configuração do spider
    name = 'icarus'

    client = pymongo.MongoClient(
        "atech-cluster.cluster-ctzxecurmuib.us-east-1.docdb.amazonaws.com:27017",
        ssl=True,
        ssl_ca_certs='rds-combined-ca-bundle.pem',
        username='atechadmin',
        password='atechb2w'
    )
    db = client[settings['DB_NAME']]
    collection = db['products']
    response = collection.find({
        'not_found': {'$exists': False},
        'sellers': {'$exists': False}
    })

    def start_requests(self):
        """ Realiza pesquisa para cada EAN retornado na query """
        for product in self.response:
            yield scrapy.Request(
                url='https://www.google.com/search?tbm=shop&q=' + product['ean'] + '&oq=' + product['ean'],
                callback=self.parse,
                meta={'product': product}
            )

    def parse(self, response):
        """ Coleta das informações do EAN pesquisado """

        product = response.meta['product']

        # Resultados da pesquisa
        items = response.xpath('//div[@id="search"]/div[@id="ires"]/ol/div[@class="g"][1]/div[@class="pslires"]')
        if len(items) == 0:
            yield {
                'spider': 'icarusnotfound',
                'ean': product['ean'],
                'not_found': True
            }

        else:

            product_item = ItemLoader(item=ProductItem(), response=response)
            product_item.add_value('ean', product['ean'])
            product_item.add_value('spider', 'icarus')

            for item in items:

                url = item.xpath('./div[@class="MCpGKc"]/h3/a/@href').extract_first()
                if 'http' not in url:
                    url = 'http://www.google.com' + url

                product_item.add_xpath('name', '//h3[@class="r"]/a/text()')
                product_item.add_xpath('description', '//div[@class="MCpGKc"]/div/text()')

                seller = dict()
                seller['name'] = response.xpath('//div[@class="A8OWCb"]/div/text()').get()
                seller['installments'] = '0'
                seller['price'] = response.xpath('//div[@class="A8OWCb"]/div/b/text()').get().replace(u'\xa0', ' ')\
                    .replace(u'R$\xa0', '').replace(u'$', '').replace(u'R$', '').replace(u'.', '').replace(u',', '.')\
                    .strip()
                seller['url'] = response.xpath('//div[@class="MCpGKc"]/h3/a/@href').get()

                # Visita página de detalhes do produto
                yield scrapy.Request(
                    url=url,
                    callback=self.parse_detail,
                    meta={'product_item': product_item, 'seller': seller}
                )

    def parse_detail(self, response):
        product_item = response.meta['product_item']

        """ Verifica se ainda está numa página do Google
        Caso não esteja, o produto possui somente 1 seller
        """
        if 'https://www.google' in response.url or 'http://www.google' in response.url:

            sellers = []
            # Tabela de sellers do produto
            sellers_rows = response.xpath('//table[@id="os-sellers-table"]/tr[@class="os-row"]')
            for seller_row in sellers_rows:
                seller = dict()

                seller['name'] = seller_row.xpath('./td[@class="os-seller-name"]/span/a/text()').extract_first().strip()

                seller['installments'] = seller_row.xpath('./td[@class="os-details-col"]/text()').extract_first() \
                    .replace(u'R$\xa0', '').strip()
                if seller['installments'] == '':
                    seller['installments'] = '0'

                seller['price'] = seller_row.xpath('./td[@class="os-price-col"]/span/text()').extract_first()\
                    .replace(u'\xa0', ' ').replace(u'R$\xa0', '').replace(u'R$', '').replace(u'.', '')\
                    .replace(u',', '.').strip()
                seller['url'] = 'https://www.google.com' + seller_row.xpath(
                    './td[@class="os-seller-name"]/span/a/@href'
                ).extract_first().strip()

                # Insere informações do seller na lista de sellers do produto
                sellers.append(seller)

            product_item.add_value('sellers', sellers)
            yield product_item.load_item()
        else:
            # Produto possui apenas 1 seller
            seller = response.meta['seller']
            seller['url'] = response.url

            product_item.add_value('sellers', [seller])
            yield product_item.load_item()

# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import datetime
import random
import string
import pymongo
import scrapy
from scrapy.spiders import CSVFeedSpider
from icarus.items import GFKItem


class ModelsearchSpider(CSVFeedSpider):
    name = 'modelsearch'
    allowed_domains = [
        'raw.githubusercontent.com',
        'www.google.com'
    ]
    start_urls = ['https://raw.githubusercontent.com/mauromattos00/randfiles/master/base_leech2.csv']
    headers = ['brand', 'model']
    delimiter = ';'

    crawledProducts = 0

    canCollect = False

    query = ''

    # ID do lote
    batchId = ''.join(random.choice(string.ascii_letters) for i in range(10))

    def parse_row(self, response, row):
        """ Process cada linha do CSV """

        #  Recupera último hash de controle
        lastcontrol = self.get_last_control()

        brand = row['brand'].replace(' ', '+')
        model = row['model'].replace(' ', '+')
        querystring = brand + '+' + model

        rowhash = brand + ':' + model
        self.query = rowhash
        if rowhash == lastcontrol:
            self.canCollect = True
            self.log('Iniciar coleta')

        if self.canCollect:

            url = 'https://www.google.com/search?tbm=shop&q=' + querystring

            yield scrapy.Request(
                url=url,
                callback=self.parse_search,
                meta={'query': rowhash}
            )

            self.crawledProducts += 1

            if self.crawledProducts == 20:

                print('\n\n')
                print('Inserindo controle')
                print('\n\n')

                yield {
                    'hash': rowhash,
                    'date': datetime.now(),
                    'control': True,
                    'flag': 'gfk'
                }
                exit()

    def parse_search(self, response):
        """ Realiza pesquisa no produto """

        item = response.xpath('//div[@id="search"]/div[@id="ires"]/ol/div[@class="g"][1]/div[@class="pslires"]')[0]

        # Recupera URL da melhor correspondência do produto (primeiro link)
        detailsurl = item.xpath('./div[@class="MCpGKc"]/h3/a/@href').extract_first()

        if 'http' not in detailsurl:
            detailsurl = 'http://www.google.com' + detailsurl

        yield scrapy.Request(
            url=detailsurl,
            callback=self.parse_detail,
            meta={'query': response.meta['query']}
        )

    def parse_detail(self, response):
        """ Processa pesquisa do produto """

        # Múltiplos sellers
        if 'https://www.google' in response.url or 'http://www.google' in response.url:
            sellers = []

            # Lista de sellers
            sellersrows = response.xpath('//table[@id="os-sellers-table"]/tr[@class="os-row"]')
            for sellerrow in sellersrows:

                seller = dict()
                seller['name'] = sellerrow.xpath(
                    './td[@class="os-seller-name"]/span/a/text()'
                ).extract_first().strip()

                seller['url'] = 'https://www.google.com' + sellerrow.xpath(
                    './td[@class="os-seller-name"]/span/a/@href') \
                    .extract_first().strip()

                sellers.append(seller)

            containsean = response.xpath('//div[@id="specs"]//td[@class="ruikzd" and contains(., "GTIN")]')
            if not containsean:

                # Link para a página de detalhes do produto
                productdetailurl = response.xpath(
                    '//div[@id="specs"]//div[@class="section-inner"]//div[@class="pag-bottom-links"]'
                    '//a[@class="pag-detail-link"]/@href'
                ).get()

                if 'http' not in productdetailurl:
                    productdetailurl = 'http://www.google.com' + productdetailurl

                yield scrapy.Request(
                    url=productdetailurl,
                    callback=self.parse_ean,
                    meta={'sellers': sellers, 'query': response.meta['query']}
                )
            else:
                eanstring = response.xpath(
                    '//div[@id="specs"]//td[@class="ruikzd" and contains(., "GTIN")]/following-sibling::td[1]/text()'
                ).get()
                ean = eanstring.split(',')[0]

                product = GFKItem()
                product['query'] = response.meta['query']
                product['ean'] = ean
                product['flags'] = [self.batchId, 'gfk']
                product['date'] = datetime.now()
                product['sellers'] = sellers

                yield product

        else:
            self.log('Produto com seller único. EAN não pode ser coletado')

    def parse_ean(self, response):
        """Recupera EAN do produto"""

        eanlist = response.xpath('//td[@class="ruikzd" and contains(., "GTIN")]/following-sibling::td[1]/text()')
        ean = eanlist.get().split(',')[0]

        product = GFKItem()
        product['query'] = response.meta['query']
        product['ean'] = ean
        product['flags'] = [self.batchId, 'gfk']
        product['date'] = datetime.now()
        product['sellers'] = response.meta['sellers']

        yield product

    def get_last_control(self):
        """ Recupera informações do último produto inserido """
        client = pymongo.MongoClient("mongodb://localhost:27017")
        db = client['icarus']
        collection = db['rodrigo']

        product = collection.find({'control': True, 'flag': 'gfk'}).sort("date", -1).limit(1)
        return product[0]['hash']
